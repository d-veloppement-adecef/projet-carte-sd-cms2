#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CmsCarteSD.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/CmsCarteSD.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../project/main.c "../../../../../../../microchip/mla/v2013-06-15/Microchip/PIC18 salloc/salloc.c" "../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD File System/FSIO.c" "../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD File System/SD-SPI.c"

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1989816886/main.o ${OBJECTDIR}/_ext/224773798/salloc.o ${OBJECTDIR}/_ext/1941720821/FSIO.o ${OBJECTDIR}/_ext/1941720821/SD-SPI.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1989816886/main.o.d ${OBJECTDIR}/_ext/224773798/salloc.o.d ${OBJECTDIR}/_ext/1941720821/FSIO.o.d ${OBJECTDIR}/_ext/1941720821/SD-SPI.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1989816886/main.o ${OBJECTDIR}/_ext/224773798/salloc.o ${OBJECTDIR}/_ext/1941720821/FSIO.o ${OBJECTDIR}/_ext/1941720821/SD-SPI.o

# Source Files
SOURCEFILES=../project/main.c ../../../../../../../microchip/mla/v2013-06-15/Microchip/PIC18 salloc/salloc.c ../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD File System/FSIO.c ../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD File System/SD-SPI.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/CmsCarteSD.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F8723
MP_PROCESSOR_OPTION_LD=18f8723
MP_LINKER_DEBUG_OPTION= -u_DEBUGCODESTART=0x1fd30 -u_DEBUGCODELEN=0x2d0 -u_DEBUGDATASTART=0xef4 -u_DEBUGDATALEN=0xb
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1989816886/main.o: ../project/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/MDD File System" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card/PIC18F" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/PIC18 salloc" -ms -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/main.o   ../project/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/224773798/salloc.o: ../../../../../../../microchip/mla/v2013-06-15/Microchip/PIC18\ salloc/salloc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/224773798" 
	@${RM} ${OBJECTDIR}/_ext/224773798/salloc.o.d 
	@${RM} ${OBJECTDIR}/_ext/224773798/salloc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/MDD File System" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card/PIC18F" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/PIC18 salloc" -ms -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/224773798/salloc.o   "../../../../../../../microchip/mla/v2013-06-15/Microchip/PIC18 salloc/salloc.c" 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/224773798/salloc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/224773798/salloc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1941720821/FSIO.o: ../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD\ File\ System/FSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1941720821" 
	@${RM} ${OBJECTDIR}/_ext/1941720821/FSIO.o.d 
	@${RM} ${OBJECTDIR}/_ext/1941720821/FSIO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/MDD File System" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card/PIC18F" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/PIC18 salloc" -ms -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1941720821/FSIO.o   "../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD File System/FSIO.c" 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1941720821/FSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1941720821/FSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1941720821/SD-SPI.o: ../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD\ File\ System/SD-SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1941720821" 
	@${RM} ${OBJECTDIR}/_ext/1941720821/SD-SPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1941720821/SD-SPI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG  -p$(MP_PROCESSOR_OPTION) -I"../project/h" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/MDD File System" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card/PIC18F" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/PIC18 salloc" -ms -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1941720821/SD-SPI.o   "../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD File System/SD-SPI.c" 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1941720821/SD-SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1941720821/SD-SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/_ext/1989816886/main.o: ../project/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1989816886" 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1989816886/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/MDD File System" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card/PIC18F" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/PIC18 salloc" -ms -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1989816886/main.o   ../project/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1989816886/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1989816886/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/224773798/salloc.o: ../../../../../../../microchip/mla/v2013-06-15/Microchip/PIC18\ salloc/salloc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/224773798" 
	@${RM} ${OBJECTDIR}/_ext/224773798/salloc.o.d 
	@${RM} ${OBJECTDIR}/_ext/224773798/salloc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/MDD File System" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card/PIC18F" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/PIC18 salloc" -ms -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/224773798/salloc.o   "../../../../../../../microchip/mla/v2013-06-15/Microchip/PIC18 salloc/salloc.c" 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/224773798/salloc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/224773798/salloc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1941720821/FSIO.o: ../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD\ File\ System/FSIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1941720821" 
	@${RM} ${OBJECTDIR}/_ext/1941720821/FSIO.o.d 
	@${RM} ${OBJECTDIR}/_ext/1941720821/FSIO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/MDD File System" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card/PIC18F" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/PIC18 salloc" -ms -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1941720821/FSIO.o   "../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD File System/FSIO.c" 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1941720821/FSIO.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1941720821/FSIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/_ext/1941720821/SD-SPI.o: ../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD\ File\ System/SD-SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1941720821" 
	@${RM} ${OBJECTDIR}/_ext/1941720821/SD-SPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1941720821/SD-SPI.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"../project/h" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/MDD File System" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card/PIC18F" -I"../../../../../../../microchip/mla/v2013-06-15/MDD File System-SD Card" -I"../../../../../../../microchip/mla/v2013-06-15/Microchip/Include/PIC18 salloc" -ms -oa- -Ou- -Ot- -Ob- -Op- -Or- -Od- -Opa-  -I ${MP_CC_DIR}\\..\\h  -fo ${OBJECTDIR}/_ext/1941720821/SD-SPI.o   "../../../../../../../microchip/mla/v2013-06-15/Microchip/MDD File System/SD-SPI.c" 
	@${DEP_GEN} -d ${OBJECTDIR}/_ext/1941720821/SD-SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1941720821/SD-SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/CmsCarteSD.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../project/18f8723_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f8723_g.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"$(BINDIR_)$(TARGETBASE).map" -w -l"../../../../Program Files (x86)/Microchip/mplabc18/v3.42/lib" -l"."  -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/CmsCarteSD.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
else
dist/${CND_CONF}/${IMAGE_TYPE}/CmsCarteSD.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../project/18f8723_g.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "..\project\18f8723_g.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"$(BINDIR_)$(TARGETBASE).map" -w -l"../../../../Program Files (x86)/Microchip/mplabc18/v3.42/lib" -l"."  -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}\\..\\lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/CmsCarteSD.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}   
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
