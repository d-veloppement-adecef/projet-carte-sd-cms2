/******************************************************************************
 *
 *               Microchip Memory Disk Drive File System
 *
 ******************************************************************************
 * FileName:        Demonstration.c
 * Dependencies:    FSIO.h
 * Processor:       PIC18
 * Compiler:        C18
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the ï¿½Companyï¿½) for its PICmicroï¿½ Microcontroller is intended and
 * supplied to you, the Companyï¿½s customer, for use solely and
 * exclusively on Microchip PICmicro Microcontroller products. The
 * software is owned by the Company and/or its supplier, and is
 * protected under applicable copyright laws. All rights are reserved.
 * Any use in violation of the foregoing restrictions may subject the
 * user to criminal sanctions under applicable laws, as well as to
 * civil liability for the breach of the terms and conditions of this
 * license.
 *
 * THIS SOFTWARE IS PROVIDED IN AN ï¿½AS ISï¿½ CONDITION. NO WARRANTIES,
 * WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
 * TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
 * IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 *****************************************************************************/


/*****************************************************************************
    Note:  This file is included to give you a basic demonstration of how the
           functions in this library work.  Prototypes for these functions,
           along with more information about them, can be found in FSIO.h
 *****************************************************************************/

//DOM-IGNORE-BEGIN
/********************************************************************
 Change History:
  Rev            Description
  ----           -----------------------
  1.2.4 - 1.2.6  No Change
  1.2.6          Add support for the PIC18F46J50_PIM
  1.3.4          Added support for PIC18F8722 on PIC18 Explorer Board
 ********************************************************************/
//DOM-IGNORE-END

#include <p18f8723.h> //biblioth�que du Microcontroleur
#include <stdlib.h> //biblioth�que 
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <i2c.h> 
#include <FSIO.h>


#pragma config OSC=HS, FCMEN=OFF, IESO=OFF, PWRT=OFF, WDT=OFF, LVP=OFF, XINST=OFF

#pragma udata udata

// MEMOIRE la taille #03301R111122223333BRA/4444/22/1COMMENTAIRESICIROB03326111525001020000456781108650888* soit 86octets
unsigned char TRAMEOUT[125];
unsigned char TRAMEIN[125];

#pragma udata udata1

char HORLOGE[12];
unsigned char TMP[125]; // PR SAV DES TRAMES

void init_spi(void);
static void spiReceiveWait();
void spi_low(void);
void spiWrite(unsigned char dat); //Write data to SPI bus;
char spiRead();
void RECEVOIR_PICSD(char t);
void ecriresurSD(char yui);
void lireTRAMESDBN(unsigned int zone, unsigned char longueur);
void lireTRAMESD(unsigned int zone, unsigned char longueur);
void excel(void);
void DESexcel(void);
//RS485

void main(void) {

    char cBcl;
    unsigned int start;

    start = 0;
    while (1) {
        spi_low();
        init_spi();



        while (!MDD_MediaDetect()); ////////////////////// d�tection de pr�sence de la carte m�moire 

        while (!FSInit()); /////////////////initialiser la carte SD

        RECEVOIR_PICSD(0x00);
        if (TRAMEIN[2] == 'W') //DEMARRAGE
            ecriresurSD('D');
        if (TRAMEIN[2] == 'V') //CYCLIQUE
            ecriresurSD('C');
        if (TRAMEIN[2] == 'M') //MINITEL
            ecriresurSD('M');
        if (TRAMEIN[2] == 'N') //MINITEL
            ecriresurSD('N');
        if (TRAMEIN[2] == 'B') //MINITEL
            ecriresurSD('B');


        if (TRAMEIN[2] == 'R') {

            lireTRAMESD(start, 117); //LIRE TRAME SD PAR 117 pour trame donnees capteurs
            DESexcel();

            start = start + 117;

            if (TRAMEOUT[1] == '$') //FIN DE FICHIER
            {
                start = 0;
            }
            RECEVOIR_PICSD(0x00); //0 91

        }

        if (TRAMEIN[2] == 'L') {
            lireTRAMESDBN(start, 99); //LIRE TRAME SD PAR 117 pour trame donnees capteurs
            for (cBcl = 0; cBcl < 100; cBcl++) {
                TRAMEOUT[cBcl] = TMP[cBcl];
            }

            start = start + 99;

            if (TRAMEOUT[1] == '$') //FIN DE FICHIER
            {
                start = 0;
            }
            RECEVOIR_PICSD(0x00);
        }
    }
} //FIN MAIN

static void spiReceiveWait() {
    while (SSP1STATbits.BF == 0); // Wait for Data Receipt complete
}

char spiRead() { // Read the received data
    spiReceiveWait(); // Wait until all bits receive
    return (SSP1BUF); // Read the received data from the buffer
}

void spiWrite(unsigned char dat) { //Write data to SPI bus
    SSP1BUF = dat;
}

void init_spi(void) {
    TRISCbits.TRISC2 = 1; // RD7/SS - Output (Chip Select)

    TRISCbits.TRISC3 = 1; // RD4/SDO - Output (Serial Data Out)
    TRISCbits.TRISC4 = 1; // RD5/SDI - Input (Serial Data In)
    TRISCbits.TRISC5 = 0; // RD6/SCK - Output (Clock)

    SSP1STATbits.SMP = 0; // input is valid in the middle of clock
    SSP1STATbits.CKE = 0; // 0  for rising edge is data capture

    SSP1CON1bits.CKP = 0; // high value is passive state

    SSP1CON1bits.SSPM3 = 0; // speed f/64(312kHz), Master
    SSP1CON1bits.SSPM2 = 1;
    SSP1CON1bits.SSPM1 = 0;
    SSP1CON1bits.SSPM0 = 1;

    SSP1CON1bits.SSPEN = 1; // enable SPI
}

void spi_low(void) {
    SSP1CON1 = 0x22; //SPI clk use the system  375Khz (32)
}

void ecriresurSD(char yui) {

    FSFILE * pointer;

    //********* Initialize Peripheral Pin Select (PPS) *************************
    //  This section only pertains to devices that have the PPS capabilities.
    //    When migrating code into an application, please verify that the PPS
    //    setting is correct for the port pins that are used in the application.

    while (!MDD_MediaDetect()); ////////////////////// d�tection de pr�sence de la carte m�moire 

    // Initialize the library
    while (!FSInit()); /////////////////initialiser la carte SD

#ifdef ALLOW_WRITES

    if ((HORLOGE[1] < 1)&&(HORLOGE[1] > 12)) {

        if (SetClockVars((2000 + HORLOGE[0]), HORLOGE[1], HORLOGE[2], HORLOGE[3], HORLOGE[4], HORLOGE[5])) ////r�glage de l'heure 
            while (1);
    } else {
        if (SetClockVars(2017, 11, 29, 15, 5, 26)) ////r�glage de l'heure 
            while (1);
    }

    ///////////////////////////////////////////////////////////////////deuxi�me fichier////////////////////////////////////////////
    //	// Create a second file
    if (yui == 'D') //DEMARRAGE
        pointer = FSfopenpgm("DEMCMS.TXT", "w");
    if (yui == 'C') //CYCLIQUE
        pointer = FSfopenpgm("SAV.TXT", "w");
    if (yui == 'M') //PAR MINITEL
        pointer = FSfopenpgm("SAVM.TXT", "w");
    if (yui == 'N') //PAR MINITEL
        pointer = FSfopenpgm("SAVN.TXT", "w");
    if (yui == 'B') //BLOC NOTES
        pointer = FSfopenpgm("SBLOC.TXT", "w");
    if (pointer == NULL)
        while (1);
    do {

        ////ICI ON RECUPERER LES DONNEES DE MICRO 1
        RECEVOIR_PICSD(0x00);
        if (yui != 'B')
            excel();

        //	// Write the string to it again
        if (yui == 'B') {
            TRAMEIN[98] = 0x0D; //SAUT DE LIGNE
            TRAMEIN[99] = 0x0A;
            TMP[0] = TRAMEIN[0];
            TMP[1] = TRAMEIN[1];
            TMP[2] = TRAMEIN[2];
            if (TMP[2] == '$')
                yui = 'B';
            if (FSfwrite((void *) TRAMEIN, 1, 99, pointer) != 99)
                while (1);
        }

        if (yui != 'B') {
            if (FSfwrite((void *) TMP, 1, 117, pointer) != 117) //90 CARACTERES TRAME #R1CM............* SANS R1CM 86
                while (1);
        }
    } while (TMP[2] != '$'); //1700 max //A CAUSE DE;
    //	// Close the file
    if (FSfclose(pointer))
        while (1);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif
    //FSremovepgm ("FILE75.txt");       //// �ffacer  un fichier.
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////


}

void lireTRAMESDBN(unsigned int zone, unsigned char longueur) {

    FSFILE * pointer;


    // Open file 1 in read mode
    pointer = FSfopenpgm("SBLOC.TXT", "r"); ///ouvrir un fichier
    if (pointer == NULL)
        while (1);

    if (FSfseek(pointer, zone, SEEK_SET)) /////////////// d�finir la posotion dans le fichier 0 DEBUT 91 SUIVANTES 91*N
        while (1);

    if (FSfread(TMP, longueur, 1, pointer) != 1) //////////////////lire des donn�es  90 ELEMENTS #CMR1....*
        while (1);

    if (FSfclose(pointer))/////////////////fermer le fichier 
        while (1);
}

void lireTRAMESD(unsigned int zone, unsigned char longueur) {

    FSFILE * pointer;

    // Open file 1 in read mode
    pointer = FSfopenpgm("DEMCMS.TXT", "r"); ///ouvrir un fichier
    if (pointer == NULL)
        while (1);



    if (FSfseek(pointer, zone, SEEK_SET)) /////////////// d�finir la posotion dans le fichier 0 DEBUT 91 SUIVANTES 91*N
        while (1);

    if (FSfread(TMP, longueur, 1, pointer) != 1) //////////////////lire des donn�es  90 ELEMENTS #CMR1....*
        while (1);

    // Close the file
    if (FSfclose(pointer))/////////////////fermer le fichier 
        while (1);
}

void RECEVOIR_PICSD(char t) {

    unsigned char oop, z, recept[12];

    oop = -1;


    ///////////////////RECEVOIR POUR METTRE EN SD

    while (!SSP1STATbits.BF);
    oop = spiRead(); // activation
    spiWrite('-'); //a
    oop = spiRead(); // activation
    spiWrite('-'); //a

    recept[0] = spiRead(); // j
    spiWrite('-'); //a
    recept[1] = spiRead(); //j
    spiWrite('-'); //a
    recept[2] = spiRead(); // m
    spiWrite('-'); //a
    recept[3] = spiRead(); //m
    spiWrite('-'); //a
    recept[4] = spiRead(); // a
    spiWrite('-'); //a
    recept[5] = spiRead(); //a
    spiWrite('-'); //a
    recept[6] = spiRead(); // h
    spiWrite('-'); //a
    recept[7] = spiRead(); //h
    spiWrite('-'); //a
    recept[8] = spiRead(); //m
    spiWrite('-'); //a
    recept[9] = spiRead(); //m
    spiWrite('-'); //a
    recept[10] = spiRead(); //ordre
    spiWrite('-'); //a
    recept[11] = spiRead(); // ordre
    spiWrite('-'); //a


    HORLOGE[0] = 10 * (recept[4] - 48)+(recept[5] - 48);
    HORLOGE[1] = 10 * (recept[2] - 48)+(recept[3] - 48);
    HORLOGE[2] = 10 * (recept[0] - 48)+(recept[1] - 48);
    HORLOGE[3] = 10 * (recept[6] - 48)+(recept[7] - 48);
    HORLOGE[4] = 10 * (recept[8] - 48)+(recept[9] - 48);
    HORLOGE[5] = 0; //secondes

    oop = -1;



    do {
        oop++;
        //RECEVOIR_PICSD(&TRAMEIN);


        if (recept[10] == 'R') {
            TRAMEIN[oop] = spiRead(); // Read the received data
            spiWrite(TRAMEOUT[oop]); // ON ENVOIE A MICRO1
            z = TRAMEOUT[oop];
        } else {
            TRAMEIN[oop] = spiRead(); // Read the received data
            spiWrite('!'); //ON RECOIT DE MICRO1
            z = TRAMEIN[oop];
        }


    } while (z != '*');

    if (recept[10] == 'R') //ON ENVOIE L'ETOILE DERNIER CARAC
    {
        TRAMEIN[oop] = spiRead(); // Read the received data
        spiWrite(TRAMEOUT[oop]); // ON ENVOIE A MICRO1
        z = TRAMEOUT[oop];

    }
    //////////////////////// FIN  DE RECEPTION POUR SD
}

void excel(void) {
    TMP[0] = TRAMEIN[0];
    TMP[1] = ';';
    TMP[2] = TRAMEIN[1]; //VOIE
    TMP[3] = TRAMEIN[2];
    TMP[4] = TRAMEIN[3];
    TMP[5] = ';';
    TMP[6] = TRAMEIN[4]; //CODAGE
    TMP[7] = TRAMEIN[5];
    TMP[8] = ';';
    TMP[9] = TRAMEIN[6]; //TYPE
    TMP[10] = ';';
    TMP[11] = TRAMEIN[7]; //CMOD
    TMP[12] = TRAMEIN[8];
    TMP[13] = TRAMEIN[9];
    TMP[14] = TRAMEIN[10];
    TMP[15] = ';';
    TMP[16] = TRAMEIN[11]; //Cconso
    TMP[17] = TRAMEIN[12];
    TMP[18] = TRAMEIN[13];
    TMP[19] = TRAMEIN[14];
    TMP[20] = ';';
    TMP[21] = TRAMEIN[15]; //Crepos
    TMP[22] = TRAMEIN[16];
    TMP[23] = TRAMEIN[17];
    TMP[24] = TRAMEIN[18];
    TMP[25] = ';';
    TMP[26] = TRAMEIN[19]; //constitution
    TMP[27] = TRAMEIN[20];
    TMP[28] = TRAMEIN[21];
    TMP[29] = TRAMEIN[22];
    TMP[30] = TRAMEIN[23];
    TMP[31] = TRAMEIN[24];
    TMP[32] = TRAMEIN[25];
    TMP[33] = TRAMEIN[26];
    TMP[34] = TRAMEIN[27];
    TMP[35] = TRAMEIN[28];
    TMP[36] = TRAMEIN[29];
    TMP[37] = TRAMEIN[30];
    TMP[38] = TRAMEIN[31];
    TMP[39] = ';';
    TMP[40] = TRAMEIN[32]; //ROBINET
    TMP[41] = TRAMEIN[33];
    TMP[42] = TRAMEIN[34];
    TMP[43] = TRAMEIN[35];
    TMP[44] = ';';
    TMP[45] = TRAMEIN[36]; //COMMENTAIRE
    TMP[46] = TRAMEIN[37];
    TMP[47] = TRAMEIN[38];
    TMP[48] = TRAMEIN[39];
    TMP[49] = TRAMEIN[40];
    TMP[50] = TRAMEIN[41];
    TMP[51] = TRAMEIN[42];
    TMP[52] = TRAMEIN[43];
    TMP[53] = TRAMEIN[44];
    TMP[54] = TRAMEIN[45];
    TMP[55] = TRAMEIN[46];
    TMP[56] = TRAMEIN[47];
    TMP[57] = TRAMEIN[48];
    TMP[58] = TRAMEIN[49];
    TMP[59] = ';';
    TMP[60] = TRAMEIN[50]; //VOIE
    TMP[61] = TRAMEIN[51];
    TMP[62] = TRAMEIN[52];
    TMP[63] = ';';
    TMP[64] = TRAMEIN[53]; //date j
    TMP[65] = TRAMEIN[54];
    TMP[66] = ';';
    TMP[67] = TRAMEIN[55]; //date M
    TMP[68] = TRAMEIN[56];
    TMP[69] = ';';
    TMP[70] = TRAMEIN[57]; //date h
    TMP[71] = TRAMEIN[58];
    TMP[72] = ';';
    TMP[73] = TRAMEIN[59]; //VOIE m
    TMP[74] = TRAMEIN[60]; //VOIE
    TMP[75] = ';';
    TMP[76] = TRAMEIN[61]; // ?
    TMP[77] = ';';
    TMP[78] = TRAMEIN[62]; //CODE
    TMP[79] = TRAMEIN[63];
    TMP[80] = ';';
    TMP[81] = TRAMEIN[64]; //POS
    TMP[82] = TRAMEIN[65];
    TMP[83] = ';';
    TMP[84] = TRAMEIN[66]; //????
    TMP[85] = TRAMEIN[67];
    TMP[86] = TRAMEIN[68];
    TMP[87] = TRAMEIN[69];
    TMP[88] = ';';
    TMP[89] = TRAMEIN[70]; //DISTANCE
    TMP[90] = TRAMEIN[71];
    TMP[91] = TRAMEIN[72];
    TMP[92] = TRAMEIN[73];
    TMP[93] = TRAMEIN[74];
    TMP[94] = ';';
    TMP[95] = TRAMEIN[75];
    TMP[96] = TRAMEIN[76]; //ETAT
    TMP[97] = ';';
    TMP[98] = TRAMEIN[77]; //VALEUR
    TMP[99] = TRAMEIN[78];
    TMP[100] = TRAMEIN[79];
    TMP[101] = TRAMEIN[80];
    TMP[102] = ';';
    TMP[103] = TRAMEIN[81]; //SEUIL
    TMP[104] = TRAMEIN[82];
    TMP[105] = TRAMEIN[83];
    TMP[106] = TRAMEIN[84];
    TMP[107] = ';';
    TMP[108] = TRAMEIN[85]; //CRC
    TMP[109] = TRAMEIN[86];
    TMP[110] = TRAMEIN[87];
    TMP[111] = TRAMEIN[88];
    TMP[112] = TRAMEIN[89];
    TMP[113] = ';';
    TMP[114] = TRAMEIN[90]; //*
    TMP[115] = 0x0D; //SAUT DE LIGNE
    TMP[116] = 0x0A;
}

void DESexcel(void) {
    TRAMEOUT[0] = TMP[0];

    TRAMEOUT[1] = TMP[2]; //VOIE
    TRAMEOUT[2] = TMP[3];
    TRAMEOUT[3] = TMP[4];

    TRAMEOUT[4] = TMP[6]; //CODAGE
    TRAMEOUT[5] = TMP[7];

    TRAMEOUT[6] = TMP[9]; //TYPE
    TRAMEOUT[7] = TMP[11]; //CMOD
    TRAMEOUT[8] = TMP[12];
    TRAMEOUT[9] = TMP[13];
    TRAMEOUT[10] = TMP[14];

    TRAMEOUT[11] = TMP[16]; //Cconso
    TRAMEOUT[12] = TMP[17];
    TRAMEOUT[13] = TMP[18];
    TRAMEOUT[14] = TMP[19];

    TRAMEOUT[15] = TMP[21]; //Crepos
    TRAMEOUT[16] = TMP[22];
    TRAMEOUT[17] = TMP[23];
    TRAMEOUT[18] = TMP[24];

    TRAMEOUT[19] = TMP[26]; //constitution
    TRAMEOUT[20] = TMP[27];
    TRAMEOUT[21] = TMP[28];
    TRAMEOUT[22] = TMP[29];
    TRAMEOUT[23] = TMP[30];
    TRAMEOUT[24] = TMP[31];
    TRAMEOUT[25] = TMP[32];
    TRAMEOUT[26] = TMP[33];
    TRAMEOUT[27] = TMP[34];
    TRAMEOUT[28] = TMP[35];
    TRAMEOUT[29] = TMP[36];
    TRAMEOUT[30] = TMP[37];
    TRAMEOUT[31] = TMP[38];

    TRAMEOUT[32] = TMP[40]; //ROBINET
    TRAMEOUT[33] = TMP[41];
    TRAMEOUT[34] = TMP[42];
    TRAMEOUT[35] = TMP[43];

    TRAMEOUT[36] = TMP[45]; //COMMENTAIRE
    TRAMEOUT[37] = TMP[46];
    TRAMEOUT[38] = TMP[47];
    TRAMEOUT[39] = TMP[48];
    TRAMEOUT[40] = TMP[49];
    TRAMEOUT[41] = TMP[50];
    TRAMEOUT[42] = TMP[51];
    TRAMEOUT[43] = TMP[52];
    TRAMEOUT[44] = TMP[53];
    TRAMEOUT[45] = TMP[54];
    TRAMEOUT[46] = TMP[55];
    TRAMEOUT[47] = TMP[56];
    TRAMEOUT[48] = TMP[57];
    TRAMEOUT[49] = TMP[58];

    TRAMEOUT[50] = TMP[60]; //VOIE
    TRAMEOUT[51] = TMP[61];
    TRAMEOUT[52] = TMP[62];

    TRAMEOUT[53] = TMP[64]; //date j
    TRAMEOUT[54] = TMP[65];

    TRAMEOUT[55] = TMP[67]; //date M
    TRAMEOUT[56] = TMP[68];

    TRAMEOUT[57] = TMP[70]; //date h
    TRAMEOUT[58] = TMP[71];

    TRAMEOUT[59] = TMP[73]; //VOIE m
    TRAMEOUT[60] = TMP[74]; //VOIE

    TRAMEOUT[61] = TMP[76]; // ?

    TRAMEOUT[62] = TMP[78]; //CODE
    TRAMEOUT[63] = TMP[79];

    TRAMEOUT[64] = TMP[81]; //POS
    TRAMEOUT[65] = TMP[82];

    TRAMEOUT[66] = TMP[84]; //????
    TRAMEOUT[67] = TMP[85];
    TRAMEOUT[68] = TMP[86];
    TRAMEOUT[69] = TMP[87];

    TRAMEOUT[70] = TMP[89]; //DISTANCE
    TRAMEOUT[71] = TMP[90];
    TRAMEOUT[72] = TMP[91];
    TRAMEOUT[73] = TMP[92];
    TRAMEOUT[74] = TMP[93];

    TRAMEOUT[75] = TMP[95];
    TRAMEOUT[76] = TMP[96]; //ETAT

    TRAMEOUT[77] = TMP[98]; //VALEUR
    TRAMEOUT[78] = TMP[99];
    TRAMEOUT[79] = TMP[100];
    TRAMEOUT[80] = TMP[101];

    TRAMEOUT[81] = TMP[103]; //SEUIL
    TRAMEOUT[82] = TMP[104];
    TRAMEOUT[83] = TMP[105];
    TRAMEOUT[84] = TMP[106];

    TRAMEOUT[85] = TMP[108]; //CRC
    TRAMEOUT[86] = TMP[109];
    TRAMEOUT[87] = TMP[110];
    TRAMEOUT[88] = TMP[111];
    TRAMEOUT[89] = TMP[112];

    TRAMEOUT[90] = TMP[114]; //*
}